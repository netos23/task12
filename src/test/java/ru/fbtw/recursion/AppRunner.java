package ru.fbtw.recursion;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Scanner;

public class AppRunner {

	void printDelimiter() {
		System.out.println("---------------------------------");
	}

	void printInfo(String comment, int index) {
		System.out.println();
		printDelimiter();
		System.out.printf("Тест №%d\n%s\n", index, comment);
		printDelimiter();
	}
	@ParameterizedTest
	@CsvFileSource(resources = "/tests.csv", numLinesToSkip = 1)
//	@CsvSource({"'(a, b, (c), d)',Тест из примера 1, 0","'(a, b, (c), d)',Тест из примера 1, 0"})
	void run(String test, String comment, int index) {
		printInfo(comment, index);
		Scanner input = new Scanner(test);
		App.setInput(input);
		App.execute();
	}
}
