package ru.fbtw.recursion;

import ru.fbtw.recursion.io.ConsoleArgInputProcessor;
import ru.fbtw.recursion.parcer.TreeNode;
import ru.fbtw.recursion.parcer.TreeParser;
import ru.fbtw.recursion.render.TreeRender;
import ru.fbtw.recursion.util.ExceptionMessages;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class App {

	private static PrintStream output = System.out;
	private static Scanner input = new Scanner(System.in);
	private static boolean isConsoleInput = true, isIsConsoleOutput = true;

	public static void main(String[] args) {
		ConsoleArgInputProcessor processor = new ConsoleArgInputProcessor();
		processor.setFlagEvent("--help", App::getHelp)
				.setFlagEvent("-i", App::setInput)
				.setFlagEvent("-o", App::setOutput);
		try {
			processor.execute(args);
		} catch (Exception e) {
			System.err.println(e.getMessage());
			System.exit(-1);
		}
		execute();
	}

	static void setInput(Scanner input) {
		isConsoleInput = false;
		App.input = input;
	}

	static void execute() {
		if (isConsoleInput) {
			System.out.println("Введиете выражение: ");
		}

		try {
			String src = input.nextLine();
			TreeNode root = TreeParser.parse(src);
			TreeRender batch = new TreeRender(output, root);

			batch.render();
		} catch (InputMismatchException ex) {
			System.err.println(ExceptionMessages.INPUT_ERR);
		} catch (Exception ex) {
			System.err.println(ExceptionMessages.UNKNOWN);
		}


	}


	/**
	 * Метод переопределяет ввод с консоли на ввод с файла
	 *
	 * @param args - в параметре 0 содержиться путь к файлу который следует загрузить
	 * @throws FileNotFoundException
	 * @throws InputMismatchException
	 */
	private static void setInput(String... args) throws FileNotFoundException, InputMismatchException {
		try {
			String filename = args[0];

			if (validateFileName(filename)) {

				File inputFile = new File(filename);
				input = new Scanner(inputFile);
			} else {
				throw new InputMismatchException(ExceptionMessages.FILE_TYPE_ERR);
			}

		} catch (FileNotFoundException ex) {
			throw new FileNotFoundException(ExceptionMessages.FILE_ERR);
		}

	}

	/**
	 * @param fileName - имя файло которое следует проверить
	 * @return - возвращает true если фаил является текстовым
	 */
	private static boolean validateFileName(String fileName) {
		return fileName.endsWith(".txt");
	}

	/**
	 * Метод переопределяет вывод в консоли на вывод в файл
	 *
	 * @param args - в параметре 0 содержиться путь к файлу в который следует сохранить
	 * @throws FileNotFoundException
	 * @throws InputMismatchException
	 */
	private static void setOutput(String... args) throws FileNotFoundException, InputMismatchException {
		try {
			String filename = args[0];


			if (validateFileName(filename)) {
				File outputFile = new File(filename);
				output = new PrintStream(outputFile);

			} else {
				throw new InputMismatchException(ExceptionMessages.FILE_TYPE_ERR);
			}

		} catch (FileNotFoundException ex) {
			throw new FileNotFoundException(ExceptionMessages.FILE_ERR);
		}
	}

	private static void getHelp(String... args) {
		String message = "Добро пожаловать в программу постройки деревьев!\n" +
				"Программа позволяет строить интересные деревья)\n" +
				"список команд: \n" +
				"--help запросить помощь\n" +
				"-i [dir] указать деррикторию из которой следует загрузить данные,\n" +
				"\tесли в headless режиме флаг не указан, то ввод необходимо произвести с консоли\n" +
				"-o [dir] указать деррикторию в которую следует сохранить данные,\n" +
				"\tесли в headless режиме флаг не указан, то вывод будет произведен в консоль\n";
		System.out.println(message);
		System.exit(0);
	}
}
