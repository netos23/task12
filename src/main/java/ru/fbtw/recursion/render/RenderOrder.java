package ru.fbtw.recursion.render;

import ru.fbtw.recursion.parcer.TreeNode;

/**
 * Класс хранящий состояние ветки, а также саму ветку и ее смещение
 */
public class RenderOrder {
	private State state;
	private int offset;
	private TreeNode target;

	public RenderOrder(int offset, TreeNode target) {
		this.offset = offset;
		this.target = target;
		this.state = State.HEADER;
	}

	public State getState() {
		return state;
	}

	public int getOffset() {
		return offset;
	}

	public TreeNode getTarget() {
		return target;
	}

	public void updateState() {
		State[] values = State.values();
		if (state.ordinal() != values.length - 1) {
			state = values[state.ordinal()+1];
		}
	}
}
