package ru.fbtw.recursion.render;

/**
 * Состояние отрисовки ветки
 */
public enum State {
	HEADER,
	MAIN_CONNECTOR,
	FLOOR,
	CONNECTORS,
	TREES;
}
