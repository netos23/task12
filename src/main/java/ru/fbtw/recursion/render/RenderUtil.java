package ru.fbtw.recursion.render;

import ru.fbtw.recursion.parcer.TreeNode;
import ru.fbtw.recursion.util.Pair;

import java.util.List;

/**
 * Вспомогательные методы для позиционирования элементов отрисовки
 */
public class RenderUtil {
	public static int getMainConnector(TreeNode node) {
		if (node.getChildren().size() == 0) {
			return getMainConnectorPosition(node.getWidth());
		} else {
			Pair<Integer> floor = getFloor(node);
			int w = getWidth(floor);
			return floor.getA() + getMainConnectorPosition(w);
		}
	}

	public static int getWidth(Pair<Integer> floor) {
		return floor.getB() - floor.getA() + 1;
	}

	private static int getMainConnectorPosition(int width) {
		return (width - (1 - width % 2)) / 2;
	}

	public static int[] getConnectors(TreeNode node) {
		List<TreeNode> leaves = node.getChildren();
		int n = leaves.size();
		int[] connectors = new int[n];

		for (int i = 0; i < n; i++) {
			connectors[i] = getMainConnector(leaves.get(i));

			if (i > 0) {
				int prevWidth = leaves.get(i - 1).getWidth();
				connectors[i] += 2 + connectors[i - 1] + prevWidth / 2;
			}
		}

		return connectors;
	}

	public static Pair<Integer> getFloor(TreeNode node) {
		int[] connectors = getConnectors(node);

		Pair<Integer> res;
		if (connectors.length == 1) {
			res = new Pair<>(connectors[0], connectors[0]);
		} else {
			res = new Pair<>(connectors[0], connectors[connectors.length - 1]);
		}

		return res;
	}
}
