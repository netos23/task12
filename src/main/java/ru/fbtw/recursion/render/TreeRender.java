package ru.fbtw.recursion.render;

import ru.fbtw.recursion.parcer.TreeNode;
import ru.fbtw.recursion.util.Pair;

import java.io.PrintStream;
import java.util.LinkedList;

public class TreeRender {
	private static final char CONNECTOR = '|';
	private static final char EMPTY = ' ';
	private static final char FLOOR = '-';
	private LinkedList<RenderOrder> renderQueue;
	private int index;
	private PrintStream out;

	public TreeRender(PrintStream out, TreeNode root) {
		this.out = out;
		renderQueue = new LinkedList<>();
		renderQueue.add(new RenderOrder(0, root));
	}

	public TreeRender(TreeNode root) {
		this(System.out, root);
	}

	/**
	 * Используется следующая модель
	 *             a					1) {@link State#HEADER} - корень ветки
	 *             |					2) {@link State#MAIN_CONNECTOR} - подсоединение корня
	 *   ---------------------- 		3) {@link State#FLOOR} - пол
	 *   |        |           |			4) {@link State#CONNECTORS} - корень ветки
	 * second    abc       abcddcba		1) {@link State#HEADER} - корень ветки
	 *            |						2) {@link State#MAIN_CONNECTOR} - подсоединение корня
	 *        ----------				3)... и тд
	 *        | |  |   |
	 *        y x uuu  8
	 *
	 * Пока очередь отрисовки не пуста: отрисовывается по-этапно дерево
	 */
	public void render() {
		index = 0;
		while (!renderQueue.isEmpty()) {
			RenderOrder renderOrder = renderQueue.pollFirst();
			TreeNode target = renderOrder.getTarget();
			State curState = renderOrder.getState();

			while (index < renderOrder.getOffset() && curState != State.TREES) {
				writeEmpty(1);
			}

			switch (renderOrder.getState()) {
				case HEADER:
					int mainConnector = RenderUtil.getMainConnector(target);
					int offset = mainConnector - target.getName().length() / 2;
					writeOffset(offset, target.getName());

					if (target.getChildren().size() == 0) {
						break;
					} else {
						renderQueue.add(renderOrder);
					}
					break;
				case MAIN_CONNECTOR:
					mainConnector = RenderUtil.getMainConnector(target);
					writeOffset(mainConnector, CONNECTOR);
					renderQueue.add(renderOrder);
					break;
				case FLOOR:
					Pair<Integer> floor = RenderUtil.getFloor(target);
					write(floor.getA(), RenderUtil.getWidth(floor), FLOOR);
					renderQueue.add(renderOrder);
					break;
				case CONNECTORS:
					int[] connectors = RenderUtil.getConnectors(target);
					for (int connector : connectors) {
						offset = connector + renderOrder.getOffset() - index;
						writeOffset(offset, CONNECTOR);
					}
					renderQueue.add(renderOrder);
					break;
				case TREES:
					int offsetIndex = renderOrder.getOffset();
					for (TreeNode treeNode : target.getChildren()) {
						RenderOrder nextOrder = new RenderOrder(offsetIndex, treeNode);
						offsetIndex += treeNode.getWidth() + 1;
						renderQueue.add(nextOrder);
					}
					break;
			}
			renderOrder.updateState();
			if (renderQueue.size() > 0
					&& curState != renderQueue.getFirst().getState()
					&& curState != State.TREES) {
				index = 0;
				System.out.println();
			}
		}
	}

	private void write(int offset, int count, char sym) {
		writeEmpty(offset);
		for (int i = 0; i < count; i++) {
			System.out.print(sym);
			index++;
		}
	}

	private void writeOffset(int offset, char sym) {
		writeOffset(offset, Character.toString(sym));
	}

	private void writeOffset(int offset, String str) {
		writeEmpty(offset);
		out.print(str);
		index += str.length();
	}

	private void writeEmpty(int count) {
		for (int i = 0; i < count; i++) {
			System.out.print(EMPTY);
			index++;
		}
	}

}
