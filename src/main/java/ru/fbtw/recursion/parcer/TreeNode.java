package ru.fbtw.recursion.parcer;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;

public class TreeNode {
	private List<TreeNode> children;
	/**
	 * Имя корня ветки
	 */
	private String name;
	/**
	 * Ширина ветки
	 */
	private int width;
	/**
	 * Поле нужно для проверки от несанкционированных добавлений в массив по асессору
	 */
	private int size;

	public TreeNode(String name) {
		this.name = name;
		children = new ArrayList<>();
		calculateWidth();
	}

	public List<TreeNode> getChildren() {
		validate();
		return children;
	}

	public String getName() {
		validate();
		return name;
	}

	public int getWidth() {
		validate();
		return width;
	}

	/**
	 * Проверка несанкционированного добавления
	 */
	private void validate() {
		if (children.size() != size) {
			throw new ConcurrentModificationException();
		}
	}

	public void add(TreeNode leaf) {
		size++;
		children.add(leaf);
		calculateWidth();

	}

	/**
	 * Обновление размера ветки
	 */
	private void calculateWidth() {
		width = 0;
		for (TreeNode leaf : children) {
			width += leaf.width;
			width++;
		}

		width--;
		width = Math.max(width, name.length());
	}
}
