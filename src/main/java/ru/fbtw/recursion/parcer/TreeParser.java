package ru.fbtw.recursion.parcer;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;

public class TreeParser {
	/**
	 * Данный метод за линейное время разбивает строку на специальные группы.
	 * <p>
	 * Преимущество данного метода над комбинацией {@link String#indexOf(int)} и {@link String#substring(int)}, то
	 * что вышеуказанные методы линейно ищут целевой индекс.
	 * <p>
	 * Проблема использования регулярных выражений в сложности решения проблемы формата (a, (c, (b)),(d),(g))
	 * В зависимости от режима (жадный, ленивый, супер жадный) иногда сложно правильно "достать"
	 * группы (c, (b)), (d),(g)
	 * <p>
	 * 1) Сначала метод отбрасывает внешние скобки, проверяя их формат
	 * 2) Затем метод разделяет выражение по запятым пока не встретит '('
	 * 3) Если встречена '(' , то будет искаться сбалансированная последовательность скобок
	 *
	 * @param src - строка которую нужно разделить
	 * @return - разделенная строка
	 */
	public static List<String> split(String src) {
		int start = 0, end = src.length();
		if (src.charAt(0) == '(') {
			if (src.charAt(src.length() - 1) == ')') {
				start++;
				end--;
			} else {
				throw new InputMismatchException();
			}
		}

		List<String> res = new ArrayList<>();
		StringBuilder tmp = new StringBuilder();
		int balance = 0;
		char curChar;
		for (int i = start; i < end; i++) {
			curChar = src.charAt(i);

			if (balance == 0) {
				switch (curChar) {
					case ' ':
						break;
					case ',':
						res.add(tmp.toString());
						tmp = new StringBuilder();
						break;
					case '(':
						balance++;
					default:
						tmp.append(curChar);
				}
			} else {
				switch (curChar) {
					case '(':
						balance++;
						break;
					case ')':
						balance--;
						break;
				}
				tmp.append(curChar);
			}
		}
		res.add(tmp.toString());
		return res;
	}

	/**
	 * Рекурсивно разделяет данные и приводит их нужному виду.
	 *
	 * @param str - исходные данные
	 * @return - полученное дерево
	 */
	public static TreeNode parse(String str) {
		List<String> parsedData = split(str);
		if (parsedData.isEmpty()) {
			throw new InputMismatchException();
		}

		TreeNode root = new TreeNode(parsedData.get(0));
		for (int i = 1; i < parsedData.size(); i++) {
			TreeNode leaf = parse(parsedData.get(i));
			root.add(leaf);
		}
		return root;
	}
}
