package ru.fbtw.recursion.io;

@FunctionalInterface
public interface ConsoleMethod {
	void execute(String ... args) throws Exception;
}
